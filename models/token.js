const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var Token = new Schema({
  userId: String,
  token: String,
  expiresIn: Number
});

module.exports = mongoose.model('Token', Token);
