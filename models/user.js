const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var User = new Schema({
  id: String,
  password: String,
  type: Number
});

module.exports = mongoose.model('User', User);
