const ping = require('ping');
const express = require('express');

const router  = express.Router();
const host = 'google.com';

function latencyRoute(req, res) {
  ping
    .promise
    .probe(host)
    .then(function onResponse(resNext) {
      resNext.token = req.token;
      res.json(resNext);
    });
}

router.get('/latency', latencyRoute);

module.exports = router;
