const express = require('express');

const User = require('../models/user');
const router  = express.Router();

function infoRoute(req, res) {
  console.log(req.params);
  var param = req.params.id;
  if (!param) {
    const message = {
      status: false,
      message: 'Id parameter is not specified.'
    };
    return res.status(400).send(message);
  }

  const query = {
    id: param
  }

  User.findOne(query, function onFinished(err, user) {
    if (err) {
      throw err;
    }

    const message = {
      typeAsString: idTypeEnum[user.type],
      token: req.token
    };

    res.json(message);
  })
}

var idTypeEnum = {
  0: "Phone",
  1: "Mail"
};

router.get('/info/:id', infoRoute);

module.exports = router;
