const AuthHelper = require('../auth-helper/authHelper');

function middlewareTokenVerifier (req, res, next) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  var newContext =

  this.req = req;
  this.res = res;
  this.next = next;

  if (token) {
    AuthHelper.verifyToken(token, onVerified.bind(this));
  }
  else {
    const message = {
      success: false,
      message: 'No token provided.'
    };

    return res.status(403).send(message);
  }
}

function onVerified(err, decoded) {
  if (err) {
    const message = {
      success: false,
      message: 'Failed to authenticate token.'
    }

    return res.json(message);
  }
  else {
    console.log(decoded);
    req.decoded = decoded;
    next();
  }
}

module.exports = middlewareTokenVerifier;
