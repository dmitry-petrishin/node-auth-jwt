const jwt     = require('jsonwebtoken');
const express = require('express');

const config          = require('../config');
const validateRequest = require('../validators/request');
const User            = require('../models/user');
const AuthHelper      = require('../auth-helper/authHelper');

const router  = express.Router();

function signinRoute(req, res) {
  const validateResult = validateRequest(req);
  if (validateResult) {
    return validateResult;
  }

  var query = {
    id: req.body.id
  };

  User.findOne(query,
    function onFinished(err, user) {
      if (err) {
        throw err;
      }

      if (!user) {
        res.json({ success: false, message: 'Authentication failed. Wrong login.' });
      }
      else if (user.password != req.body.password) {
        res.json({ success: false, message: 'Authentication failed. Wrong password.' });
      }
      else {
        const token = AuthHelper.releaseToken(user.id);
        res.json({
          success: true,
          token: AuthHelper.releaseToken(user.id),
        });
      }
    });
}

router.post('/signin', signinRoute);

module.exports = router;
