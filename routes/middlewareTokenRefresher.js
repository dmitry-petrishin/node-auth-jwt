const AuthHelper = require('../auth-helper/authHelper');

function middlewareTokenRefresher (req, res, next) {
  console.log(req.decoded.id);
  var id = req.decoded.id;

  req.token = AuthHelper.refreshToken(id);

  next();
}

module.exports = middlewareTokenRefresher;
