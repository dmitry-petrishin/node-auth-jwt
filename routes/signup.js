const express = require('express');
var router  = express.Router();

const validateRequest = require('../validators/request');
const User            = require('../models/user');
const AuthHelper      = require('../auth-helper/authHelper');

function signupRoute(req, res) {
  const validation = validateRequest(req, res);
  if (validation) {
    return validation;
  }

  const model = req.body;

  const idType = defineIdType(model.id);
  if (idType < 0) {
    return res.status(400).send({
      success: false,
      message: 'Unable to define login type.'
    });
  }

  var user = new User({
    id: model.id,
    password: model.password,
    type: idType
  });

  user.save(function onSaved(err) {
    if (err) {
      throw err;
    }

    console.log('User saved successfully');

    var message = {
      success: true,
      token: AuthHelper.releaseToken(user.id),
    }
    res.status(200).json(message);
  })
}

router.post('/signup', signupRoute);

module.exports = router;

// private functions
function defineIdType(id) {
  var result = -1;

  if (isPhoneType(id)) {
    result = 0;
  }
  else if (isMailType(id)) {
    result = 1;
  }

  return result;
}

function isMailType(id) {
  const index = id.indexOf("@");
  var result = false;

  if (index > -1) {
    result = true;
  }

  return result;
}

function isPhoneType(id) {
  const isNumber = /^\d+$/.test(id);
  let isNumber1 = /^[+][\d]+$/.test(id);
  var result = false;

  if (isNumber) {
    result = true;
  }
  console.log(isNumber1);
  return result;
}
