function validateRequest(req, res) {
  var model = req.body;

  // TODO: extract messages to separate place
  if (!model) {
    return res.status(400).send({
      success: false,
      message: 'Body is empty.'
    });
  }
  if (!model.id) {
    return res.status(400).send({
      success: false,
      message: 'Login hasn\'t been provided.'
    });
  }
  if (!model.password) {
    // TODO: check password complexity for signup
    return res.status(400).send({
      success: false,
      message: 'Password hasn\'t been provided.'
    });
  }
}

module.exports = validateRequest;
