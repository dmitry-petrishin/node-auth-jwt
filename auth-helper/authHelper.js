const jwt = require('jsonwebtoken');

const config = require('./../config');

function AuthHelper() {
  this.releaseToken = function(id) {
    const payload = {
      id: id
    };

    const expiration = {
      expiresIn: Math.floor(Date.now() / 1000) + (60 * 10),
    };

    return jwt.sign(payload, config.secret, expiration);
  };

  this.refreshToken = function(id) {
    // expire old token
      return this.releaseToken(id);
  };

  this.verifyToken = function(token, onVerified) {
    jwt.verify(token, config.secret, onVerified);
  };

  this.deleteToken = function(token) {

  };

  this.deleteAllTokens = function(user) {

  };
};

module.exports = new AuthHelper();
