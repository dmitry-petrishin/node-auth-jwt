const bodyParser = require('body-parser');
const config     = require('./config');
const express    = require('express');
const mongoose   = require('mongoose');
const morgan     = require('morgan');

const infoRoute    = require('./routes/info');
const latencyRoute = require('./routes/latency');
const signinRoute  = require('./routes/signin');
const signupRoute  = require('./routes/signup');
//const signoutRoute = require('./routes/signout');
const middlewareTokenVerifier  = require('./routes/middlewareTokenVerifier');
const middlewareTokenRefresher = require('./routes/middlewareTokenRefresher');

const app    = express();
const router = express.Router();
const port   = process.env.PORT || 8080;

// connect to database
mongoose.connect(config.database);

//using of body-parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// using of morgan
app.use(morgan('dev'));

// routes
router.use(
  signinRoute,
  signupRoute,
  middlewareTokenVerifier,
  middlewareTokenRefresher,
  infoRoute,
  latencyRoute);
app.use('/api', router);

// start of the server
app.listen(port);
console.log('Service is rised at http://localhost:' + port);
